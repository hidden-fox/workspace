for v in authentication-service data-engine elastic-service dev-ops elastic-service inferschema sparkjob-service billing-service common metadata-service reporting-service data-lab
do
    echo ${v};
    cd ${v};
    git pull origin master;
    cd ..;
done
