
DIR="sparkjob-service"
if [ -d "$DIR" ]; then
  echo "Already cloned [${DIR}], skipping"  
else
  echo "Cloning [${DIR}]";
  git clone git@gitlab.com:hidden-fox/sparkjob-service.git;  
fi

DIR="authentication-service"
if [ -d "$DIR" ]; then
  echo "Already cloned [${DIR}], skipping"  
else
  echo "Cloning [${DIR}]";
  git clone git@gitlab.com:hidden-fox/authentication-service.git;
fi

DIR="common"
if [ -d "$DIR" ]; then
  echo "Already cloned [${DIR}], skipping"  
else
  echo "Cloning [${DIR}]";
  git clone git@gitlab.com:hidden-fox/common.git;
fi

DIR="dev-ops"
if [ -d "$DIR" ]; then
  echo "Already cloned [${DIR}], skipping"  
else
  echo "Cloning [${DIR}]";
  git clone git@gitlab.com:hidden-fox/dev-ops.git;
fi

DIR="elastic-service"
if [ -d "$DIR" ]; then
  echo "Already cloned [${DIR}], skipping"  
else
  echo "Cloning [${DIR}]";
  git clone git@gitlab.com:hidden-fox/elastic-service.git;
fi

DIR="web-ui"
if [ -d "$DIR" ]; then
  echo "Already cloned [${DIR}], skipping"  
else
  echo "Cloning [${DIR}]";
  git clone git@gitlab.com:hidden-fox/web-ui.git;
fi

DIR="metadata-service"
if [ -d "$DIR" ]; then
  echo "Already cloned [${DIR}], skipping"  
else
  echo "Cloning [${DIR}]";
  git clone git@gitlab.com:hidden-fox/metadata-service.git;
fi

DIR="data-engine"
if [ -d "$DIR" ]; then
  echo "Already cloned [${DIR}], skipping"
else
  echo "Cloning [${DIR}]";
  git clone git@gitlab.com:hidden-fox/data-engine.git;
fi

DIR="billing-service"
if [ -d "$DIR" ]; then
  echo "Already cloned [${DIR}], skipping"
else
  echo "Cloning [${DIR}]";
  git clone git@gitlab.com:hidden-fox/billing-service.git;
fi

DIR="inferschema"
if [ -d "$DIR" ]; then
  echo "Already cloned [${DIR}], skipping"
else
  echo "Cloning [${DIR}]"
  git clone git@gitlab.com:hidden-fox/inferschema.git;
fi

DIR="reporting-service"
if [ -d "$DIR" ]; then
  echo "Already cloned [${DIR}], skipping"
else
  echo "Cloning [${DIR}]"
  git clone git@gitlab.com:hidden-fox/reporting-service.git
fi

DIR="data-lab"
if [ -d "$DIR" ]; then
  echo "Already cloned [${DIR}], skipping"
else
  echo "Cloning [${DIR}]"
  git clone git@gitlab.com:hidden-fox/data-lab.git
fi